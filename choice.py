import date

#adds variables needed to initiate a response from the player
class Choice():

    def __init__(date, text, points, reaction, reaction_sprite):

        self.date = date
        self.text = text
        self.points = points
        self.reaction = reaction
        self.reaction_sprite = reaction_sprite
