# Characters
from character import Character
from gender import Gender
from date import Date
from choice import Choice

#Contains each character, their dialogue and options that the player can choose to respond with
characters = [
	Character("Josh",Gender.M, "music", [
			Date("Hey! So what sort of music do you like?",
				Choice("I don’t like music they’re just noise to me.",
						0, "Oh. Okay.", "disappointed"),
				Choice("I like classical music like Mozart and Beethoven.",
						50, "Hey, that's pretty good."),
				Choice("I like acoustic music. I find guitar sounds sexy."
						100, "Hey! I play guitar! ;)")
				),
# Each player response has an assigned point value, for the "effectiveness" of the line
			Date("I make music, want a link my Soundcloud?",
				Choice("Soundcloud? Isn’t that for amateurs?",
						0, "Hey, man. Give some respect for us small time musicians.", "disappointed"),
				Choice("I'll check it out.",
						50, "Nice. It's joshmaeksmusic."),
				Choice("Wow, that's so cool!"
						100, "Thanks!")
				),

			Date("What do you think about ?",
				Choice("Soundcloud? Isn’t that for amateurs?",
						0, "Hey, man. Give some respect for us small time musicians.", "disappointed"),
				Choice("I'll check it out.",
						50, "Nice. It's joshmaeksmusic."),
				Choice("Wow, that's so cool!"
						100, "Thanks!")
				),
		])
	
	Character("Lewis",Gender.M, "corny", [
			Date("Hi! Is this the speed dating place? I think I’m running late.",
				Choice("Yeah, dumbass.", 0,"It's just a joke no need to be rude.", "disappointed"),
				Choice("Yeah! Nice to meet you.", 50, "Awww nice to meet you too!"),
				Choice(f"Hi running late, I'm {username}, and yeah it is", 100, "I see you're a person of jokes too huh hehe")
				
				),

			Date("I go hiking, you know, it’s pretty in-tents.",
				Choice("I don’t like going outside.", 0, "That's kinda boring if you ask me.", "disappointed"),
				Choice("Can you take me sometime?", 50, "I would love to!"),
				Choice("Let's go and PEAK a trail together", 100, "Now that's witty hahaha sure")

				),

			Date("Wanna get some coffee after this? I feel something brewing between us.",
				Choice("No.", 0, "Awwww too bad", "disappointed"),
				Choice("*blush* heehee sure!", 50, "It's settled then!"),
				Choice("You’re such a TEAse. ;)", 100, "Hahahaha I can't help it, especially with you.")

				),

			Date("Can I have your number? I’ll call you later.",
				Choice("No, I’m not interested.", 0, "Awwww okay feels bad.", "disappointed"),
				Choice("Sure!", 50, "Sweet!"),
				Choice(f"Don’t call me later. Call me {username}. (But in all seriousness, sure *½ wink*)", 100, "But can I call you mine? ehe")

				),
		])

	Character("Marq",Gender.M, "alien", [
			Date("¿Dónde estoy? ¿Qué esta pasando? ¿Por qué me miras así? (Where am I? What's happening? Why are you looking at me like that?)",
				Choice("Are you an illegal immigrant? I'm calling the police.", 0,"....!!!", "disappointed"),
				Choice("Are you okay? do you need some help?", 50, "tha..nks..."),
				Choice(f"Estamos en un simulador de citas rápidas, Soy {name} por cierto. (We're in a speed dating sim, I'm {name} btw"., 100, "....:O")
				
				),

			Date("*whispers* I think the linguistic transposer should be in proper operation.Greetings my fellow relatively middle aged human. How is it dangling?",
				Choice("Why are you talking that way? Dangling? What’s wrong with you?", 0, "I'm sorry if the talking disturbs you...", "disappointed"),
				Choice("Dangling? Oh…. I’m ok if i understand you correctly", 50, "*whispers* Oh wow someone understands me heehee"),
				Choice("I like the way you talk. I’m fine if you’re asking, thanks.", 100, "You like how I talk? T-that's a first...")

				),

			Date("I like to use my vision spheres to look up at the stars. I’d really like to explore more out there.",
				Choice("uhhh- vision spheres? What a funny way of naming your eyes.", 0, "H-hey don't tease me!", "disappointed"),
				Choice("Oh. I like the stars too.", 50, "They're pretty right? I could stare at them forever."),
				Choice("I’m seeing stars when I look at you <3.", 100, "*blush* S-stop it you're embarrasing me...")

				),

			Date("Do you believe that aliens exist?",
				Choice("I think it’s just some bullshit made by the government to fool the people that space actually exists but in reality this “earth” we call is just a flat disk encapsulated in a dome to not let any normal people see the real truth outside of it in order to continue the cycle of what makes us live and reproduce normally to create more consumers for the benefit of those capitalist pigs who hoard all the money and suck the literal life out of the people. Also I’m a vegan and an anti-vaccine advocist.", 0, "Oh... okay then (sad)", "disappointed"),
				Choice("They’re pretty cool who knows when they will invade our planet.", 50, "We're- I mean they're fascinating creatures am I right?"),
				Choice("I actually want to meet one someday.", 100, "I'm positive you'll do hehehe")

				),

			Date("Uh, want an experience that’s… *reads writing on palm* outside this hemisphere? L-let’s take my ship sometime.",
				Choice("Uh, no hablo IMMIGRANT", 0, "Ouch jeez I-i'm only offering you.", "disappointed"),
				Choice("Um, not sure what you meant, but sounds cool!", 50, "You'll see when you come along with me."),
				Choice("Sure, I think it’ll be out of this world? hehe", 100, "It's settled then! I'm so excited I can't wait!")

				),
		])
]