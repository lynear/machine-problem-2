from enum import Enum
#Needed to add the gender of each character
class Gender(Enum):
    male = 0
    female = 1
    nonbinary = 2
