import pyglet

#default window size is 640x480
width, height = 1280, 720
window = pyglet.window.Window(width=width, height=height)
background= pyglet.image.SolidColorImagePattern((255,255,255,255)).create_image(width, height)
data = background.get_image_data().get_data('RGB', width*3)
new_image = b''
time_passed = 0
sentence = "Oni-chan~~~"
text_scroll_start = False
curr_index = 0
min_tick = 1/60
choices = ["1. Meron akong alaga", "2. Aso at Pusa", "3. Hindi nag-aaway, nakakatuwa"]
answered = False
char_image = pyglet.image.load('loli_default.png')
char = pyglet.sprite.Sprite(char_image, x = 840, y = 80)
char.scale = 0.4

@window.event
def on_draw():
    window.clear()
    background.blit(0, 0)
    label.draw()
    name.draw()
    choice.draw()
    char.draw()

@window.event
def on_mouse_press(x, y, button, modifier):
    global text_scroll_start
    global answered

    print(str(x) + ' ' + str(y))

    if (x > window.width // 4 and x < window.width * 3 // 4 and y > window.height * 3 // 4 and y < window.height):
        text_scroll_start = True

    if (y > ((window.height // 24) - 5) and y < ((window.height // 6) + 5)):
        answered = True

def tick(dt):
    global time_passed
    time_passed += dt
    update()

def update():
    global curr_index
    global text_scroll_start
    global min_tick
    global time_passed
    global answered
    global sentence

    if (text_scroll_start and time_passed > min_tick):
    # Emulate how text appears in most VNs
    # if (time_passed > min_tick):
        if (curr_index < len(sentence)):
            label.text += sentence[curr_index]
            curr_index += 1
            time_passed = 0

        # Rewrite text
        else:
            if (answered):
                label.text = ""
                sentence = "BIGYAN NG 1 MILLION"
                curr_index = 0
                answered = False

label = pyglet.text.Label("",           #Character Lines
        font_name = "Times New Roman",
        font_size = 25,
        x=window.width//2, y = window.height * 3 // 4,
        anchor_x="center", anchor_y="baseline", multiline = True, width = window.width // 2, align = "center")

name = pyglet.text.Label("Willie",      #Character Name
        font_name = "DejaVu Sans Mono",
        font_size = 15,
        color= (0,0,0,255),
        x=window.width // 6, y = window.height * 11 // 12,
        anchor_x="right", anchor_y="center", align = "center")

choice_string = ""

for i in choices:
    choice_string += i + '\n'

choice_string.rstrip('\n')

choice = pyglet.text.Label(choice_string,   #Choices for the player
        font_name = "DejaVu Sans Mono",
        font_size = 15,
        color= (5,5,5,255),
        x=window.width // 1.7, y = window.height // 12,
        anchor_x="center", anchor_y="center", align = "left", 
        multiline = True, width = window.width)

if (__name__ == "__main__"):
    pyglet.clock.schedule_interval(tick, 1/60)
    pyglet.app.run()